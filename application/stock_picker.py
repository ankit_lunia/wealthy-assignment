import os
import sys
sys.path.append(os.getcwd())

import csv
import dateutil.parser as parser
from difflib import get_close_matches
import bisect
import math
from application.utils import get_date_input, parse_date, process_csv, calculate_mean_std_deviation_buy_and_sell_date

def stock_picker():
    if len(sys.argv) != 2:
        print('Invalid input! \nPlease use shown input format.\nEx - python stock_picker.py "path/to/csv"')
        return
    file_path = sys.argv[1]
    file_ext = file_path.split('.')[-1]

    if file_ext != 'csv':
        print('Please use csv files only!')
        return
    
    stock_data = []
    stock_names = []

    stock_data, stock_names = process_csv(file_path)

    stock_data.sort(key=lambda x: x[1])
    dates = [sd[1] for sd in stock_data]

    while True:
        stock_name = raw_input('Welcome Agent! Which stock you need to process:-').lower()
        start_date = None
        end_date = None

        if stock_name not in stock_names:
            stock_name = get_close_matches(stock_name, stock_names, n=1)
            
            if len(stock_name) < 1:
                print('='*50)
                print('Stock name not found!')
                print('='*50)
                continue
            stock_name = stock_name[0]
            ans = raw_input('Oops! Do you mean {}? y or n:-'.format(stock_name.upper()))

            if ans.lower() == 'y':
                start_date = get_date_input('From which date you want to start:-')
                end_date = get_date_input('Till which date you want to analyze:-')
            else:
                print('-'*50)
                ans = raw_input('Do you want to continue? y or n:-')
                if ans.lower() == 'y':
                    continue
                else:
                    print('-'*50)
                    print('Bye bye, See you again!' )
                    break
        
        if not start_date and not end_date:
            start_date = get_date_input('From which date you want to start:-')
            end_date = get_date_input('Till which date you want to analyze:-')
        
        if start_date > end_date:
            print('='*50)
            print('Start date must be less than End date!')
            print('Please try again!')
            print('='*50)
            continue
        
        start_stock_index = bisect.bisect_left(dates, start_date)
        end_stock_index = bisect.bisect_right(dates, end_date)
        
        stock_data_between_dates = [sd for sd in stock_data[start_stock_index:end_stock_index] if sd[0] == stock_name]

        if len(stock_data_between_dates):
            calculate_mean_std_deviation_buy_and_sell_date(stock_data_between_dates, start_date, end_date)

            print('='*50)
            ans = raw_input('Do you want to continue? y or n:-')
            if ans.lower() == 'y':
                print('='*50)
                continue
            else:
                print('-'*50)
                print('Bye bye, See you again!' )
                break
        else:
            print('Nothing found between dates {} - {}'.format(start_date, end_date))
        

if __name__ == '__main__':
    stock_picker()