import dateutil.parser as parser
import csv
import sys
import math

def get_date_input(message):
    while True:
        date = raw_input(message)
        return parse_date(date, dayfirst=True, yearfirst=True)

def parse_date(date, dayfirst=False, yearfirst=False):
    try:
        return parser.parse(date, dayfirst=True, yearfirst=True).date()
    except Exception as e:
        print('Invalid date! Please re-enter date.')

def process_csv(file_path):
    stock_data = []
    stock_names = []

    print('Processing csv file...')

    with open(file_path, 'rb') as stock_input:
        csv_reader = csv.reader(stock_input)
        csv_reader.next()
        csv_reader = list(csv_reader)

        count = 0
        row_count = len(csv_reader)

        for row in csv_reader:
            count += 1
            per = count*100/row_count
            sys.stdout.write('\r{}%'.format(per))
            
            if row[0].lower() not in stock_names:
                stock_names.append(row[0].lower())
            
            data = (row[0].lower(), parse_date(row[1]), row[2])
            stock_data.append(data)
    
    print('\nFile is ready')
    
    return stock_data, stock_names

def calculate_mean_std_deviation_buy_and_sell_date(stock_data_between_dates, start_date, end_date):
    print(stock_data_between_dates)
    mean_val_of_stocks = sum([ float(sd[2]) for sd in stock_data_between_dates ])
    standard_deviation = math.sqrt(sum([ math.pow(float(sd[2])-mean_val_of_stocks, 2) for sd in stock_data_between_dates ])/len(stock_data_between_dates))

    max_profit = None
    buy_date = None
    sell_date = None
    if len(stock_data_between_dates) > 1:
        for i, min_sd in enumerate(stock_data_between_dates):
            for max_sd in stock_data_between_dates[i+1:]:
                if not max_profit:
                    max_profit = float(max_sd[2]) - float(min_sd[2])
                    buy_date = str(min_sd[1])
                    sell_date = str(max_sd[1])
                elif max_profit < float(max_sd[2]) - float(min_sd[2]):
                    max_profit = float(max_sd[2]) - float(min_sd[2])
                    buy_date = str(min_sd[1])
                    sell_date = str(max_sd[1])
    else:
        buy_date = stock_data_between_dates[0][1]
        sell_date = stock_data_between_dates[0][1]
        max_profit = 0
    
    if max_profit < 0:
        max_profit = 'minimum loss of {}'.format(max_profit)
    
    print('-'*50)
    print('Here is your result:-')
    print('Mean:- {}'.format(mean_val_of_stocks))
    print('Std:- {}'.format(standard_deviation))
    print('Buy date:- {}'.format(buy_date))
    print('Sell date:- {}'.format(sell_date))
    print('Profit:- {}'.format(max_profit))
    print('-'*50)
